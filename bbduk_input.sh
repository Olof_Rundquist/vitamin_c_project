#!/bin/bash -l

# Find all of samples
Replicate1=($(find ./VitC*/Replicate1/*/*fastq.gz -type f | sort -z))
Replicate1_rerun=($(find ./VitC*/Replicate1_rerun/*/*fastq.gz -type f | sort -z))
Replicate2=($(find ./VitC*/Replicate2/*/*fastq.gz -type f | sort -z))
Replicate3=($(find ./VitC*/Replicate3/*/*fastq.gz -type f | sort -z))

mkdir ./BBduk # make the output directories
mkdir ./BBduk/Replicate1
mkdir ./BBduk/Replicate1_rerun
mkdir ./BBduk/Replicate2
mkdir ./BBduk/Replicate3

for index in ${!Replicate1[*]}
do
    sbatch ./bbduk_run.sh ./BBduk/Replicate1/${Replicate1[$index]##./*/} "${Replicate1[$index]}" 
    sbatch ./bbduk_run.sh ./BBduk/Replicate1_rerun/${Replicate1_rerun[$index]##./*/} "${Replicate1_rerun[$index]}" 
    sbatch ./bbduk_run.sh ./BBduk/Replicate2/${Replicate2[$index]##./*/} "${Replicate2[$index]}" 
    sbatch ./bbduk_run.sh ./BBduk/Replicate3/${Replicate3[$index]##./*/} "${Replicate3[$index]}" 
done
