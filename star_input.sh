#!/bin/bash -l

mkdir ./aligned

rep1_groups=($(for i in $(find ./BBduk/Replicate1*/*fastq.gz -type f) ; do echo $i | cut -d _ -f1,2 | cut -d / -f4 ; done | uniq))
rep2_groups=($(for i in $(find ./BBduk/Replicate2/*fastq.gz -type f) ; do echo $i | cut -d _ -f1,2 | cut -d / -f4 ; done | uniq))
rep3_groups=($(for i in $(find ./BBduk/Replicate3/*fastq.gz -type f) ; do echo $i | cut -d _ -f1,2 | cut -d / -f4 | cut -d - -f3 ; done | uniq))
# All have twelve groups this way like they should.
# Note that the groupnames in group 3 are different and in a diffrent order.
# Reorder the groups to the same order. Later replace the group 3 group names for consitency
rep1_groups=$(for i in 5 1 3 4 0 2 11 7 9 10 6 8 ; do echo ${rep1_groups[i]:0:-1} ; done)
rep2_groups=$(for i in 5 1 3 4 0 2 11 7 9 10 6 8 ; do echo ${rep2_groups[i]} ; done)
rep3_groups=$(echo ${rep3_groups[*]} | xargs -n1 | sort | xargs) 
# Note this also converst the groups from a list to a line delimited string. Important to know for looping purposes. 

for i in $rep1_groups
do
    reads=$(find ./BBduk/Replicate1*/*fastq.gz -type f | grep ${i} | tr "\n" "," )
    sbatch star_SE.sh "${reads}" ./aligned/Rep1_${i%%_*}
done

#for i in $rep2_groups
#do
#    reads=$(find ./BBduk/Replicate2/*fastq.gz -type f | grep ${i} | tr "\n" "," )
#    sbatch star_SE.sh "${reads}" ./aligned/Rep2_${i%%_*}
#done

# And for rep3. Note the file renaming
#count=0
#for i in $rep3_groups
#do
#    reads=$(find ./BBduk/Replicate3/*fastq.gz -type f | grep ${i} | tr "\n" "," ) 
#    group=($(for i in $rep1_groups ; do echo $i ; done))
#    sbatch star_SE.sh "${reads}" ./aligned/Rep3_${group[$count]%%_*}
#    count=$(expr $count + 1)
#done 
