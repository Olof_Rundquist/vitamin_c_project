#!/bin/bash -l

#SBATCH -J STAR
#SBATCH -n 32
#SBATCH -t 00:30:00


STAR --runThreadN 32 --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --readFilesCommand zcat --genomeDir /proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index --readFilesIn ./trimmed_galore/Replicate1/L-100VC_S3_L001_R1_001_trimmed.fq.gz,./trimmed_galore/Replicate1/L-100VC_S3_L002_R1_001_trimmed.fq.gz,./trimmed_galore/Replicate1/L-100VC_S3_L003_R1_001_trimmed.fq.gz,./trimmed_galore/Replicate1/L-100VC_S3_L004_R1_001_trimmed.fq.gz,./trimmed_galore/Replicate1_rerun/L-100VC_S2_L001_R1_001_trimmed.fq.gz,./trimmed_galore/Replicate1_rerun/L-100VC_S2_L002_R1_001_trimmed.fq.gz,./trimmed_galore/Replicate1_rerun/L-100VC_S2_L003_R1_001_trimmed.fq.gz,./trimmed_galore/Replicate1_rerun/L-100VC_S2_L004_R1_001_trimmed.fq.gz, --outTmpDir ./aligned/Rep1_L-100VC.tmp. --outFileNamePrefix ./aligned/Rep1_L-100VC

sbatch stringtie_run.sh ./aligned/Rep1_L-100VC*bam
