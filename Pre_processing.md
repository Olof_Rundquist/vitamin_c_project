## Data Preprocessing

### Setting up
The necessary packages was installed into an Anaconda virtual enviroment.
```bash
conda create NGS_env
conda install -y -c Bioconda fastqc multiqc BBmap samtools STAR stringtie 
source activate NGS_env
```
### Initial quality controll and read trimming
To verify the quality of the data an initial quality controll was performed with Fastqc on all aquired RNA sequencing files.

#### fastqc.sh
```bash
#!/bin/bash -l

#SBATCH -J fastqc
#SBATCH -n 32
#SBATCH -t 02:00:00

mkdir ./fastqc
#Note that fastqc uses about 250 mb of ram per thread so each node with 96gb ram across 32 cores can support 384 threads. 
fastqc -t 100 -o ./fastqc ./*/*/*/*fastq.gz

cd ./fastqc

multiqc *zip

```

Based on the quality report (Fastqc_report_pre_trimming.html) the data was shown to contain sequencing adapters 
and have a rather serious GC content bias problem. The GC bias was two-fold with sharp peaks at 100 and 0% GC in some samples as well as non-uniform peaks around 50% GC.
Closer examination of the individual Fasqc reports showed that the sharp peaks at the begining and end of the curve was from mono-nucleotide reads (such as poly-A).
These reads would never align to the genome and as such this problem was ignored.
The source of the non-uniform peaks at 50% GC was not clear from the indvidual reports but assumed to be caused by the adapter content.
To solve this problem the reads were trimmed of sequencing adapters and poor quality bases with BBduk from the BBmap package.

#### bbduk_input.sh
```bash
#!/bin/bash -l

# Find all of samples
Replicate1=($(find ./VitC*/Replicate1/*/*fastq.gz -type f | sort -z))
Replicate1_rerun=($(find ./VitC*/Replicate1_rerun/*/*fastq.gz -type f | sort -z))
Replicate2=($(find ./VitC*/Replicate2/*/*fastq.gz -type f | sort -z))
Replicate3=($(find ./VitC*/Replicate3/*/*fastq.gz -type f | sort -z))

mkdir ./BBduk # make the output directories
mkdir ./BBduk/Replicate1
mkdir ./BBduk/Replicate1_rerun
mkdir ./BBduk/Replicate2
mkdir ./BBduk/Replicate3

for index in ${!Replicate1[*]}
do
    sbatch ./bbduk_run.sh ./BBduk/Replicate1/${Replicate1[$index]##./*/} "${Replicate1[$index]}" 
    sbatch ./bbduk_run.sh ./BBduk/Replicate1_rerun/${Replicate1_rerun[$index]##./*/} "${Replicate1_rerun[$index]}" 
    sbatch ./bbduk_run.sh ./BBduk/Replicate2/${Replicate2[$index]##./*/} "${Replicate2[$index]}" 
    sbatch ./bbduk_run.sh ./BBduk/Replicate3/${Replicate3[$index]##./*/} "${Replicate3[$index]}" 
done
```

##### bbduk_run.sh
```bash
#!/bin/bash -l

#SBATCH -n 32
#SBATCH -t 00:05:00

bbduk.sh threads=32 in=$2 out=$1 ref=adapters ktrim=r trimq=20
```

After this fastqc was run again on the trimmed files (Fastqc_report_post_trimming_BBduk.html). 
Examination of the resulting report and individual fastqc reports showed that BBduk successfully removed all adapter sequences. 
This also fixed the GC content bias resulting in a wide uniform GC peak at 50% for all samples.

### Alignment

To align the reads the splice aware sequencing aligner STAR (https://github.com/alexdobin/STAR) was used.
As can be seen the script is rather complicated. This is because I decided to name the output alignment files based on their experimental condition.
In most cases this was in the file name but not for replicate 3 so I had to use the underlying metadata (see file: Sample_metadata.xlsx).  

#### star_input.sh
```bash
#!/bin/bash -l

mkdir ./aligned

rep1_groups=($(for i in $(find ./BBduk/Replicate1*/*fastq.gz -type f) ; do echo $i | cut -d _ -f1,2 | cut -d / -f4 ; done | uniq))
rep2_groups=($(for i in $(find ./BBduk/Replicate2/*fastq.gz -type f) ; do echo $i | cut -d _ -f1,2 | cut -d / -f4 ; done | uniq))
rep3_groups=($(for i in $(find ./BBduk/Replicate3/*fastq.gz -type f) ; do echo $i | cut -d _ -f1,2 | cut -d / -f4 | cut -d - -f3 ; done | uniq))
# All have twelve groups this way like they should.
# Note that the groupnames in group 3 are different and in a diffrent order.
# Reorder the groups to the same order. Later replace the group 3 group names for consitency
rep1_groups=$(for i in 5 1 3 4 0 2 11 7 9 10 6 8 ; do echo ${rep1_groups[i]} ; done)
rep2_groups=$(for i in 5 1 3 4 0 2 11 7 9 10 6 8 ; do echo ${rep2_groups[i]} ; done)
rep3_groups=$(echo ${rep3_groups[*]} | xargs -n1 | sort | xargs) 
# Note this also converst the groups from a list to a line delimited string. Important to know for looping purposes. 

for i in $rep1_groups
do
    reads=$(find ./BBduk/Replicate1*/*fastq.gz -type f | grep ${i} | tr "\n" "," )
    sbatch star_SE.sh "${reads}" ./aligned/Rep1_${i%%_*}
done

for i in $rep2_groups
do
    reads=$(find ./BBduk/Replicate2/*fastq.gz -type f | grep ${i} | tr "\n" "," )
    sbatch star_SE.sh "${reads}" ./aligned/Rep2_${i%%_*}
done

# And for rep3. Note the file renaming
count=0
for i in $rep3_groups
do
    reads=$(find ./BBduk/Replicate3/*fastq.gz -type f | grep ${i} | tr "\n" "," ) 
    group=($(for i in $rep1_groups ; do echo $i ; done))
    sbatch star_SE.sh "${reads}" ./aligned/Rep3_${group[$count]%%_*}
    count=$(expr $count + 1)
done 

```

##### star_SE.sh
```bash
#!/bin/bash -l

#SBATCH -J STAR
#SBATCH -n 32
#SBATCH -t 00:15:00

samples=${1}
outname=${2}
genomedir=/proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index

STAR --runThreadN 32 --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --readFilesCommand zcat --genomeDir $genomedir --readFilesIn ${samples} --outTmpDir ${outname}.tmp. --outFileNamePrefix ${outname}

sbatch stringtie_run.sh ${outname}*bam
```

The quality of the alignment was checked by compilling the logs produced by star with multiqc (Alignment_report.html).
The alignment rate was good, between 75 and 90%, and the read depth decent at 20 to 40 million reads per sample.

### Gene quantification and transcript assembley

To quantify the the gene expression from the read alignment the transcript assembler Stringtie was used.

#### Stringtie_input.sh
```bash
#!/bin/bash -l

for i in ./aligend/*bam
do
	sbatch stringtie.sh ${i}
done
```

##### Stringtie_run.sh
```bash
#!/bin/bash -l

#SBATCH -J Stringtie
#SBATCH -n 32
#SBATCH -N 1
#SBATCH -t 00:10:00

bam=$1
gtf=/proj/lassim/reference_genomes_OR/human/hg38_refseq/GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gtf
outdir=$(echo $bam | cut -d . -f1,2)

stringtie -eB -p 32 -G $gtf -o ${outdir}/expression.gtf $bam
```

Finally the expression counts were sumarised to a count matrix of all samples using the script prepDE.py provided by the stringtie webpage.

