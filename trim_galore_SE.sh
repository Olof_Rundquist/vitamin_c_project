#!/bin/bash -l

#SBATCH -t 02:00:00
#SBATCH -n 1
#SBATCH -J Trim_galore

files=${2} 

trim_galore -o ${1} --fastqc ${files} 

#NEB Adapter
#CAAGCAGAAGACGGCATACGAGATNNNNNNGTGACTGGAGTTCAGACGTGTGCTCTTCCGATC
