## Vitamin C RNA_Seq analysis

This document will go through the R analysis and its results. 
For the data preprocessing pipeline and quality analysis see Pre_processing.md.
For the R code see Clean_R_code.R. Note the clean detail is relative.

### Initial setup

The data was loaded in as standard for limma and almost all possible comparisons were run. Broadly the comparisons fell into four groups:
```
1. Comparing both cell lines together for each condition against Control.
2. Comparing each cell line separately for each condition against Control
3. Comparing treatment with Catalse against Mock
4. Comparing the different contrast to each other. This was to try to find genes that behaved differently for each cell line.
```
These comparisons were evaluated using upset plots. 
An Upset plot is similar to a venn diagram but expresses the intersections and sets as bars rather than circles.
For the outcome of this see the plots in the upsetplots folder. Based on these plots all comparisons except Group2 were not analyzed further. Why?:
```
Group1: Since the cell lines reacted quite differently to Vitamin C treatment these contrast were not considered usable.
Group3: No significant genes
Group4: Difficult to interpret and very few significant genes.
```
As such I focused on how each cell line reacted to the Vitamin C treatment and if Catalase treatment made any difference. 

### Enrichment analysis
To pick apart the effect of each treatment the significantly expressed genes were divided into four groups:
```
1. Highly Upregulated genes. Log2FC > 0.5
2. Highly Downregulated genes. Log2FC < -0.5
3. Marginally Upregulated genes. 0.25 < Log2FC < 0.5
4. Marginally Downregulated genes. -0.25 > Log2FC > -0.5
```
And an enrichment analysis was performed. 
These cutoffs were based on the density of the logFC. 
The upper cutoff of 0.5 was set based on an observation of a small peak of genes above said cutoff for the Loucy cells at 100 and 3000 µM vitamin C.
The marginal cutoff was set to study marginal effects of Vitamin C. 0.25  was chosen as the lower cutoff since it fell outside the majority of the main peak centered at 0.
The cutoff for downregulation were set to mirror the cutoffs for upregulation.
See figure bellow for demonstration of how the cutoffs were applied.

![Inline image](Density_plots/logFC_example_density_plot.png)

The following enrichment analysis was run through the R packages clusterprofiler and ReactomePA. 
The gene lists for Up and down regulated genes were compared to the databases Gene Ontology (Biological processes (BP), cellular compartment (CC) and Molecular function (MF)), Kegg and ReactomeDB (React).
The result was summarized as a heatmap of the Top 10 significant pathways for each comparison.
 
![Inline image](Pathway_heatmaps/High_pathways_heatmaps.png)

![Inline image](Pathway_heatmaps/Marginal_pathways_heatmaps.png)

For 100 µM Vitamin C (VC) in Loucy a strong upregulation of genes linked to amino acid catabolism can be seen 
and a marginal upregulation of genes associated with T-cell activation.
For Sup-T1 100 µM VC nothing beyond a few cell compartment were significant.
Among the highly downregulated genes no pathways were significant beyond a few cellular compartments in Loucy.
Marginal downregulation could be seen for genes associated with differentiation, mainly HOX-domain transcription factors, in Loucy.

At 3000 µM VC in Loucy multiple genes associated with iron response pathways were strongly upregulated. 
Most striking is the upregulation of genes associated with ferroptosis, an iron dependent type of cell death caused by an accumulation of oxidized lipids.
This is consistent with the high toxicity of Vitamin C at 3000 µM. 
It's worth noting that none of these pathways are upregulated when adding catalse and
instead an upregualtion of genes associated with T-cell differentiation can be seen.

For Sup-T1 the association between iron, cell death and catalse rescue is less clear. 
In Sup-T1 at 3000 µM VC a very significant (P < 1e-20) upregulation of cholesterol biosynthesized can be seen invariable of catalse treatment.
For catalse a marginal upregulation can be seen for genes associated with Toll-like receptor responses.  
For Downregulated genes again the pathways are shared between catalse and no catalase 
mainly showing downregulation of genes associated with pathways for cholesterol transport/homeostasis, cell differentiation/development and B-cell receptor signaling.

### Shared genes between the cell lines

In general the response between the cell lines and conditions was very divergent. 

![Inline image](upset_plots/Upset_Shared_Upregulated_VC.png)
![Inline image](upset_plots/Upset_Shared_Downregulated_VC.png)

There is only one genes shared between all highly upregulated gene sets: FBXL5

FBXL5 is a part of the proteasome, it is supposedly important for antigen presentation. This is also the only shared gene for 100 µM VC.

Among the marginally upregulated the only shared gene for 100 µM was LRRC41, 
a poorly characterized gene involved in protosomal degradation of proteins

And among the downregulated for 100 µM VC the only shared gene was SLC40A1. An Iron carrier protein.

There were more shared Upregulated genes for 3000 µM VC, 56 in total, however they were not enriched for any specific pathway.
A table of all shared genes (will be) is avilable. See x file.

### DNA Methylation and histone modification analysis

The DNA methylation/histone modification analysis was carried out thusly: 
RRBS DNA methylation data for 1 kb upstream of the transcription start site (TSS) was acquired for Loucy and SUP-T1 from the CCLE database.
Histone modification data was found for Loucy through CistromeDB and was mainly from the Encode project. Other databases were also checked such as chipbase, array express and GEO. 
The only exception was the H3K27 acetylation data that came from an independent study. No histone modification data was found for Sup-T1.
The DNA methylation data was linked to genes directly by using the provided locus id in the file from CCLE. 
The histone modification peaks were linked to the promotors of genes using the tool chipseeker. 
Promotors were defined as +- 5000bps from TSS and all peaks within said distance were assigned to the gene. 
In the case of multiple peaks linking to the same gene the average signal (fold enrichment) of all peaks was reported.

For some of the histone modifications there was two equally good (judged by the quality control markings provided by cistromeDB) available studies. 
In this case a union of the two dataset was created. If the peak was present in both datasets the average signal was reported. 
Genes with no assigned peaks were considered as zero.
For the DNA methylation data since it was sequencing based the data was considered as missing if not reported. 

To test if DNA methylation status or histone modification influence how a gene was affected by vitamin C 
the disitribution of fold enrichment for Upregulated genes was compared against downregulated genes using a Willcox signed ranked test.
The theory was that since vitamin C is a co-factor for demethylating enzymes such as the TETs and jumojin methyltransferases the 
upregulated genes should have a higher signal on average than the downregulated genes if the mark was repressive and higher if it was activating. 
Since adding vitamin C in theory should remove the mark.
 
At 100 µM vitamin C there was no significant difference in fold enrichment between the up and downregulated genes for any modification. 
However the average signal tended to be higher for the downregulated genes. 
At 3000 µM the distribution of fold enrichment between up and downregulated genes was almost the same as at 100 µM. 
However since the sample size was much larger there was a significant difference between up and downreglated genes (P < 0.05).
This difference however was not consistent with the hypothesis that if the mark was repressive the fold enrichment should be higher in the upregulated genes, 
instead it was higher in the downregulated genes. See below plots.
Note that Up and Down (ex: H3K27me3_Up and H3K27me3_Down) referes to the genes going Up and down and not the modfication. 

![Inline image](Boxplots/Up_vs_Down_transposed_Loucy_VC_100_vs_Control_methylation_boxplot_loucy.png)
![Inline image](Boxplots/Up_vs_Down_transposed_Loucy_VC_3000_vs_Control_methylation_boxplot_loucy.png)

For DNA methylation there was no significant difference between up and down regulated genes at any contrast.

### Turning the analysis on the side

I then tested turning the question around and instead asking if having the modification or not affected the logFC distribution of the genes.
The cutoff between having vs not having the modification was set based on the density of the fold enrichment for each modification. 
All modification had a bimodal signal distribution so the cutoff was set in between the two peaks. See example below for H3K4me2.

![Inline image](Density_plots/H3K4me2_density_example.png)

Once the cutoff had been set the genes were divided into two groups based on if they had the modfication or not and ploted against each other in a density plot.

Based on these density plots the marks fell into three significant categories:

1. Marks genes affected by vitamin C: H3K9me2 and me3. H3K9me3 especially.
2. Marks genes not affected by vitamin C: All other histone marks.
3. Marks neither: DNA methylation.

This is very interesting since H3K9me3 specifically is a target of KDM4A (previously JMJD2A) which like the TETs is vitamin C dependent.
Something to note though is that out of 44 significant genes for 100 µM VC only three (HDC, SELP and PYHIN1) are H3K9 methylated so this enrichment mainly 
concerns genes that are not significantly differentially expressed.

![Inline image](Density_plots/H3K9me3_Expression_density_Loucy_VC100.png)


![Inline image](Density_plots/H3K27me3_Expression_density_Loucy_VC100.png)


![Inline image](Density_plots/Loucy_mC_Expression_density_Loucy_VC100.png)


