#!/usr/bin/env R
# -*- coding: utf-8 -*-

# Created on Mon Sep  30 16:03:00 2019

# @author: Olof Rundquist

# Packages
library(edgeR)
library(stats)
library(data.table)
library(stringr)
library(limma)
library(RColorBrewer)
library(UpSetR)
library(gplots)
library(ggplot2)
library(DOSE)
library(clusterProfiler)
library(ReactomePA)
library(TxDb.Hsapiens.UCSC.hg38.knownGene)
library(org.Hs.eg.db)
library(rtracklayer)
library(ChIPseeker)
library(scales)

Plot_density <- function(dge=cds, cpm_cutoff=5, xrows=3){
	pdf("Density_plot_raw_vs_filtered.pdf", width =7, height = 7)
	cpm <- cpm(dge)
	lcpm <- cpm(dge, log=TRUE) # calculate log counts per million.
	nsamples <- ncol(dge)
	col <- brewer.pal(nsamples, "Paired")
	par(mfrow=c(1,2))
	plot(density(lcpm[,1]), col=col[1], lwd=2, ylim=c(0,0.21), las=2,
		 main="", xlab="")
	title(main="A. Raw data", xlab="Log-cpm")
	abline(v=0, lty=3)
	for (i in 2:nsamples){
		 den <- density(lcpm[,i])
		 lines(den$x, den$y, col=col[i], lwd=2)
		}
	#legend("topright", colnames(dge$counts)[4:length(colnames(dge$counts))], text.col=col, bty="n") # takes up to much space.

	# exlude lowly expressed genes. Reload DGE until the distribution becomes uniform.
	keep.exprs <- rowSums(cpm>cpm_cutoff)>=xrows
	dge <- dge[keep.exprs,, keep.lib.sizes=FALSE]

	lcpm <- cpm(dge, log=TRUE)
	plot(density(lcpm[,1]), col=col[1], lwd=2, ylim=c(0,0.21), las=2,
		 main="", xlab="")
	title(main="B. Filtered data", xlab="Log-cpm")
	abline(v=0, lty=3)
	for (i in 2:nsamples){
		 den <- density(lcpm[,i])
		 lines(den$x, den$y, col=col[i], lwd=2)
		}
	#legend("topright", colnames(gc)[4:length(colnames(gc))], text.col=col, bty="n")
	dev.off()
	return(dge)
	}

plot_MDS_heat <- function(dge=cds){
pdf( "MDS_plot.pdf" , width = 7 , height = 7 ) # in inches
plotMDS.DGEList( dge , main = "MDS Plot for Count Data", labels = colnames( dge$counts ), col=as.numeric(as.factor(paste(edesign$Treat, edesign$Treat2, sep="_"))) )
dev.off()
# The main diffrence is the diffrence between cell lines. Plot the cell lines seperetly.
pdf( "MDS_plot_loucy.pdf" , width = 7 , height = 7 ) # in inches
plotMDS.DGEList( dge$counts[, colnames(dge$counts) %like% "_L."] , main = "MDS Plot for Count Data", 
				labels = colnames( dge$counts )[colnames(dge$counts) %like% "_L."], col=as.numeric(as.factor(paste(edesign$Treat, edesign$Treat2, sep="_"))) )
dev.off()
pdf( "MDS_plot_Sup.pdf" , width = 7 , height = 7 ) # in inches
plotMDS.DGEList( dge$counts[, colnames(dge$counts) %like% "_S."] , main = "MDS Plot for Count Data", 
				labels = colnames( dge$counts )[colnames(dge$counts) %like% "_S."], col=as.numeric(as.factor(paste(edesign$Treat, edesign$Treat2, sep="_"))) )
dev.off()


#Make a heatmap
#select data for the 1000 most highly expressed genes
select = order(rowMeans(dge$counts), decreasing=TRUE)[1:1000]
highexprgenes_counts <- dge$counts[select,]

# heatmap with sample name on X-axis
svg(file="High_expr_genes.heatmap.svg", height=14, width=14)
heatmap(highexprgenes_counts, col=topo.colors(50), margin=c(10,6))
dev.off()
# PCA
# transpose the data to have variables (genes) as columns
data_for_PCA <- t(highexprgenes_counts)
dim(data_for_PCA)

## calculate MDS (matrix of dissimilarities)
mds <- cmdscale(dist(data_for_PCA), k=3, eig=TRUE)  
# k = the maximum dimension of the space which the data are to be represented in
# eig = indicates whether eigenvalues should be returned

# transform the Eigen values into percentage
eig_pc <- mds$eig * 100 / sum(mds$eig)
# plot the PCA
pdf(file="PCA_PropExplainedVariance.pdf")
barplot(eig_pc,
     las=1,
     xlab="Dimensions", 
     ylab="Proportion of explained variance (%)", y.axis=NULL,
     col="darkgrey")
dev.off()

## calculate MDS
mds <- cmdscale(dist(data_for_PCA), k=4) # Performs MDS analysis 
#Samples representation
pdf(file="PCA_Dim1vsDim2.pdf")
plot(mds[,1], -mds[,2], type="n", xlab="Dimension 1", ylab="Dimension 2", main="", col=as.numeric(as.factor(paste(edesign$Treat, edesign$Treat2, sep="_"))))
text(mds[,1], -mds[,2], rownames(mds), cex=0.8) 
dev.off()
#Samples dim 3 and 4 
pdf(file="PCA_Dim3vsDim4.pdf")
plot(mds[,3], -mds[,4], type="n", xlab="Dimension 3", ylab="Dimension 4", main="", col=as.numeric(as.factor(paste(edesign$Treat, edesign$Treat2, sep="_"))))
text(mds[,3], -mds[,4], rownames(mds), cex=0.8) 
dev.off()
}

Run_diff_analysis <- function(dge = cds, eR_contrasts=Group_table, cont.matrix=cont.matrix_batch, Outname="batch"){
pdf("limma_voom_plot.pdf", height=7, width=7)
v <- voom(dge, eR_contrasts, plot=TRUE)
dev.off()
vfit <- lmFit(v, eR_contrasts)
vfit <- contrasts.fit(vfit, contrasts=cont.matrix)
efit <- eBayes(vfit)
# get the result for all contrast. Use a for loop and make a table.
limma_result <- data.frame(row.names=rownames(dge$counts))
limma_logFC <- data.frame(row.names=rownames(dge$counts))
for (x in colnames(cont.matrix)){
    result <- topTreat(efit, coef=x, p.value=1, n="inf", sort.by="none")
    limma_result[x] <- result$adj.P.Val
	limma_logFC[x] <- result$logFC
    } 
write.csv(limma_result, file=paste("FDR_limma", Outname, "result.csv", sep="_"))
write.csv(limma_logFC, file=paste("logFC_limma", Outname, "result.csv", sep="_"))
result <- list(limma_result, limma_logFC)
names(result) <- c("limma_FDR", "limma_logFC")
return(result)
	}

make_upset <- function(data_df = result$limma_FDR, columns =seq(7,16), cutoff=0.05, filename="Limma_upset_no_batch_correction.svg"){
	listInput <- unlist(list(rep("", times=length(columns))))
	names(listInput) <- colnames(data_df)[columns]
	for (i in names(listInput)){
		if (length(rownames(data_df[data_df[i] <= 0.05,])) == 0) {next} 
		listInput[i] <- list(rownames(data_df[data_df[i] <= 0.05,]))
		}
	svg(file=filename, height=14.4, width=25.6)
	print(upset(fromList(listInput), order.by = "freq", matrix.color = "seagreen3", main.bar.color = "orange2", sets.bar.color = "lightblue", nsets=10,  text.scale=1.5, nintersects=10))
	dev.off()
	}

run_enrichment <- function(genes=gene_vector, prefix="signficant Tags", contrast="i", DT=pathway_table){
    count=1+length(DT[DT[,Description != ""], Description])
	# Reactome
    ego <- enrichPathway(
        gene = genes,
        organism = 'human',
        maxGSSize = 500,
        pvalueCutoff = 1
      )
	for (i in rownames(ego@result)){
		if (paste("React:", ego@result[i, "Description"]) %in% DT[,Description]){
			if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
				DT[DT[, Description == paste("React:", ego@result[i, "Description"])], (contrast) := ego@result[i, "p.adjust"]]
			}
			if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
			if (ego@result[i, "p.adjust"] >= 0.05)(break)
			next
		}
		if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
			DT[count, "Description" := paste("React:", ego@result[i, "Description"])]
			DT[count, (contrast) := ego@result[i, "p.adjust"]]
			count=count+1
			}
		if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
		if (ego@result[i, "p.adjust"] >= 0.05)(break)
		} 
	write.csv(ego@result, paste("./Enrichment_tables/Enrich", prefix, "reactome_Enrichment_table.csv", sep="_"))
    # Kegg
    ego <- enrichKEGG(
        gene = genes,
        organism = 'hsa',
        keyType="ncbi-geneid",
        maxGSSize = 500,
        pvalueCutoff = 1
      )
	for (i in rownames(ego@result)){
		if (paste("Kegg:", ego@result[i, "Description"]) %in% DT[,Description]){
			if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
				DT[DT[, Description == paste("Kegg:", ego@result[i, "Description"])], (contrast) := ego@result[i, "p.adjust"]]
			}
			if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
			if (ego@result[i, "p.adjust"] >= 0.05)(break)
			next
		}
		if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
			DT[count, "Description" := paste("Kegg:", ego@result[i, "Description"])]
			DT[count, (contrast) := ego@result[i, "p.adjust"]]
			count=count+1
			}
		if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
		if (ego@result[i, "p.adjust"] >= 0.05)(break)
		} 
	write.csv(ego@result, paste("./Enrichment_tables/Enrich", prefix, "Kegg_Enrichment_table.csv", sep="_"))
    # BP
    ego <- enrichGO(
        gene = genes,
        maxGSSize = 500,
        ont="BP",
        OrgDb="org.Hs.eg.db",
        pvalueCutoff = 1
      )
	for (i in rownames(ego@result)){
		if (paste("BP:", ego@result[i, "Description"]) %in% DT[,Description]){
			if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
				DT[DT[, Description == paste("BP:", ego@result[i, "Description"])], (contrast) := ego@result[i, "p.adjust"]]
			}
			if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
			if (ego@result[i, "p.adjust"] >= 0.05)(break)
			next
		}
		if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
			DT[count, "Description" := paste("BP:", ego@result[i, "Description"])]
			DT[count, (contrast) := ego@result[i, "p.adjust"]]
			count=count+1
			}
		if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
		if (ego@result[i, "p.adjust"] >= 0.05)(break)
		} 
	write.csv(ego@result, paste("./Enrichment_tables/Enrich", prefix, "BP_Ontology_Enrichment_table.csv", sep="_"))
	    ego <- enrichGO(
        gene = genes,
        maxGSSize = 500,
        ont="MF",
        OrgDb="org.Hs.eg.db",
        pvalueCutoff = 1
      )
	for (i in rownames(ego@result)){
		if (paste("MF:", ego@result[i, "Description"]) %in% DT[,Description]){
			if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
				DT[DT[, Description == paste("MF:", ego@result[i, "Description"])], (contrast) := ego@result[i, "p.adjust"]]
			}
			if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
			if (ego@result[i, "p.adjust"] >= 0.05)(break)
			next
		}
		if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
			DT[count, "Description" := paste("MF:", ego@result[i, "Description"])]
			DT[count, (contrast) := ego@result[i, "p.adjust"]]
			count=count+1
			}
		if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
		if (ego@result[i, "p.adjust"] >= 0.05)(break)
		} 
	write.csv(ego@result, paste("./Enrichment_tables/Enrich", prefix, "MF_Ontology_Enrichment_table.csv", sep="_"))
	ego <- enrichGO(
        gene = genes,
        maxGSSize = 500,
        ont="CC",
        OrgDb="org.Hs.eg.db",
        pvalueCutoff = 1
      )
	for (i in rownames(ego@result)){
		if (paste("CC:", ego@result[i, "Description"]) %in% DT[,Description]){
			if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
				DT[DT[, Description == paste("CC:", ego@result[i, "Description"])], (contrast) := ego@result[i, "p.adjust"]]
			}
			if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
			if (ego@result[i, "p.adjust"] >= 0.05)(break)
			next
		}
		if (ego@result[i, "p.adjust"] <= 0.05 && sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) != "1"){
			DT[count, "Description" := paste("CC:", ego@result[i, "Description"])]
			DT[count, (contrast) := ego@result[i, "p.adjust"]]
			count=count+1
			}
		if (sapply(str_split(ego@result[i, "GeneRatio"], "/"), "[", 1) == "1"){next}
		if (ego@result[i, "p.adjust"] >= 0.05)(break)
		} 
	write.csv(ego@result, paste("./Enrichment_tables/Enrich", prefix, "CC_Ontology_Enrichment_table.csv", sep="_"))
    }

make_pathway_heatmap <- function(DT=pathway_Up , prefix="", TopX=20, Cex=1){
	DM <- as.matrix(log(DT[, 2:length(colnames(DT))], 10))
	rownames(DM) <- DT[, Description]
	Top=c()
	for (i in colnames(DM)){
		geneList <- DM[,i]
		names(geneList) <- rownames(DM)
		geneList <- sort(geneList)
		geneList <- geneList[geneList <= 0.05]
		Top <- c(Top, head(names(geneList), n=TopX)) 
		}
	Top <- unique(Top)
	my_palette <- colorRampPalette(c("red","white"))(n = 20)
	svg(paste(prefix, as.character(TopX), "pathways_heatmap.svg", sep="_"), width=24, height=15)
	heatmap.2(DM[Top,], col=my_palette, margin=c(20,55), breaks=seq(-10, 0, length.out=21), srtCol=45, cexCol=1.5, key=TRUE, keysize = 0.5, trace="none", Colv=NULL, cexRow=Cex, dendrogram ="none", main=paste(gsub(prefix, "_", " "), "pathway heatmap"))
	dev.off()
	}

Enrichment_analysis <- function(prefix="", logFC=data_matrix, FDR=data_matrix, upper_cutoff=1e06, lower_cutoff=0.5){
#Run the enrichment and get the upregulated genes.
# Make the data tables and result lists
pathway_Up <- data.table(Description = vector("character", 5000))
pathway_Down <- data.table(Description = vector("character", 5000)) 
for (i in colnames(logFC)){pathway_Up[, (i) := c(rep(1, times=5000))] ; pathway_Down[, (i) := c(rep(1, times=5000))]}
Upregulated_genes <- list()
Downregulated_genes <- list()
for (i in colnames(logFC)){
	geneList <- logFC[,i]
	names(geneList) <- rownames(logFC)
	geneList <- geneList[names(geneList) %in% rownames(FDR[FDR[,i] <= 0.05,])]
	Up <- names(geneList[geneList < upper_cutoff & geneList >= lower_cutoff])
	Down <- names(geneList[geneList > -upper_cutoff & geneList <= -lower_cutoff])
	Upregulated_genes[[i]] <- gff[match(Up, gff$ID), "Name"]
	Downregulated_genes[[i]] <- gff[match(Down, gff$ID), "Name"]
	if (length(Up) > 10){run_enrichment(genes=gff[match(Up, gff$ID), "Dbxref"], prefix=paste(prefix, "Upregulated", i, sep="_"), contrast=i, DT=pathway_Up)}
	if (length(Down) > 10){run_enrichment(genes=gff[match(Down, gff$ID), "Dbxref"], prefix=paste(prefix, "Downregulated", i, sep="_"), contrast=i, DT=pathway_Down)}
	}
pathway_Up <- pathway_Up[pathway_Up[, Description != ""]]
pathway_Down <- pathway_Down[pathway_Down[, Description != ""]]
replace_NA <- function(DT) {
  for (i in names(DT))
    DT[is.na(get(i)), (i):=1]
}
replace_NA(pathway_Up)
replace_NA(pathway_Down)
write.csv(pathway_Up, paste("./Pathway_heatmaps/Pathway_table", prefix, "Upregulated.csv", sep="_"))
write.csv(pathway_Down, paste("./Pathway_heatmaps/Pathway_table", prefix, "Downregulated.csv", sep="_"))

make_pathway_heatmap(DT=pathway_Up, prefix=paste("./Pathway_heatmaps/", prefix, "_Upregulated", sep=""), TopX=10, Cex=1)
make_pathway_heatmap(DT=pathway_Down, prefix=paste("./Pathway_heatmaps/", prefix, "_Downregulated", sep=""), TopX=10, Cex=1)

return(list(pathway_Up, pathway_Down, Upregulated_genes, Downregulated_genes))
	}

plot_DNA_methylation_pattern <- function(genes=high_FC$Upregulated_genes[[2]], meth=methylation_data_table, cell_line="LOUCY_HAEMATOPOIETIC_AND_LYMPHOID_TISSUE", filename=""){
	# Add the methylation signal to the genes
	if (all(!genes %in% meth[,SYMBOL]) == TRUE){geneList <- rep(NA, times=length(genes)); names(geneList) <- genes; return(geneList)}
	geneList <- vector("numeric", length(genes))
	names(geneList) <- genes
	for (i in genes){
		if (i %in% meth[,SYMBOL]){
			geneList[i] <- mean(meth[SYMBOL == i, get(cell_line)])
			}
		else {
			geneList[i] <- NA
			}
		}
	svg(filename)
	hist(geneList, breaks=20)
	dev.off()
	return(geneList)
}

DNA_Methylation_analysis <- function(dataset=high_FC$Upregulated_genes, prefix=""){
	result <- vector("numeric", length(names(dataset)))
	names(result) <- names(dataset)
	methylation_list <- list()
	for (i in names(dataset)){
		if (identical(dataset[[i]], character(0))){result[i] <- NA; methylation_list[[i]] <- NA ; next}
		if (i %like% "Loucy"){
			genes <- plot_DNA_methylation_pattern(genes=dataset[[i]], meth=meth, cell_line="LOUCY_HAEMATOPOIETIC_AND_LYMPHOID_TISSUE", filename=paste("./Methylation_histograms/DNA_Methylation_", i, ".svg", sep=""))
			if (all(is.na(genes))){result[i] <- NA}
			else {result[i] <- wilcox.test(genes, Loucy_background)$p.value}
			methylation_list[[i]] <- genes
			}
		else {
			genes <- plot_DNA_methylation_pattern(genes=dataset[[i]], meth=meth, cell_line="SUPT1_HAEMATOPOIETIC_AND_LYMPHOID_TISSUE", filename=paste("./Methylation_histograms/DNA_Methylation_", i, ".svg", sep=""))
			if (all(is.na(genes))){result[i] <- NA}
			else {result[i] <- wilcox.test(genes, Sup_T1_background)$p.value}
			methylation_list[[i]] <- genes
			}
	}
	# Make a boxplot for Loucy and Sup_T1 each
	methylation_list[["Loucy_background"]] <- Loucy_background
	methylation_list[["Sup_T1_background"]] <- Sup_T1_background
	svg(paste(prefix, "methylation_boxplot_loucy.svg", sep="_"))
	par(mar=c(18,4,4,4))
	boxplot(value~Var2, x = methylation_list[c(2,3,4,5,11)] , col = "lightblue", ylab = "Methylation",frame.plot = FALSE, las=2)
	stripchart(value~Var2 ,x = methylation_list[c(2,3,4,5)] ,  col="black" , method = "overplot" , pch=19 , vertical = T , add=T)
	title(paste("Loucy", gsub("_", " ", prefix), "Methylation"))
	dev.off()
	svg(paste(prefix, "methylation_boxplot_Sup_T1.svg", sep="_"))
	par(mar=c(18,4,4,4))
	boxplot(value~Var2, x = methylation_list[c(7,8,9,10,12)] , col = "lightblue", ylab = "Methylation",frame.plot = FALSE, las=2)
	stripchart(value~Var2 ,x = methylation_list[c(7,8,9,10)] ,  col="black" , method = "overplot" , pch=19 , vertical = T , add=T)
	title(paste("Sup_T1", gsub("_", " ", prefix), "Methylation"))
	dev.off()
	return(list(result, methylation_list))

}

make_df_from_list <- function(data) {
    nCol <- max(vapply(data, length, 0))
    data <- lapply(data, function(row) c(row, rep(NA, nCol-length(row))))
    data <- matrix(unlist(data), nrow=length(data), ncol=nCol, byrow=TRUE)
	return(data.frame(data))
}

make_df_from_nested_list <- function(data) {
    nRow <- max(vapply(data, length, 0))
    data <- lapply(data, function(row) c(row, rep(NA, nRow-length(row))))
	return(as.data.frame(data))
}

annotate_peaks <- function(peaks="./H3K27me3/ENCFF048PKS.bed"){
    txdb <- TxDb.Hsapiens.UCSC.hg38.knownGene
	peaks <- readPeakFile(peaks)
    peakAnno <- annotatePeak(peaks, tssRegion = c(-3000,3000),assignGenomicAnnotation=TRUE, level="gene",TxDb=txdb,overlap="TSS",addFlankGeneInfo = TRUE, flankDistance = 5000, annoDb="org.Hs.eg.db")
    png(paste("H3K27me3_Loucy_Encode_annotation_pie.png", sep=""))
    plotAnnoPie(peakAnno)
    dev.off()
    annon.out <- data.frame(peakAnno)
    annon.out <- annon.out[!(colnames(annon.out) %like% "X")]
    return(annon.out)
}

add_histone_data <- function(peak_file="./Histone_data/H3K27me3_ENCFF048PKS.bed", RNA=RNA_seq, missing_values=0, mod="H3K27me3", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak")){
	anno=annotate_peaks(peaks=peak_file)
	if ("fold_enrichment" %in% names(anno)){
	promo <- anno[anno[, "annotation"] %like% "Promoter",]
	promo <- as.data.table(promo) # Convert to data table
	signal_for <- unique(unlist(sapply(promo[,flank_geneIds], function(x) strsplit(x, ";")))) 
	geneList <- vector("numeric", length(rownames(RNA)))
	names(geneList) <- gff[match(rownames(RNA), gff$ID), "Dbxref"]
		for (i in names(geneList)){
		if (i %in% signal_for){
			geneList[i] <- mean(promo[promo[, flank_geneIds %like% eval(i)], fold_enrichment])
			}
		else {
			geneList[i] <- missing_values
			}
		}	
	}
	else {
	colnames(anno) <- c(colnames(anno)[1:5], col_names, colnames(anno)[13:length(colnames(anno))])
	promo <- anno[anno[, "annotation"] %like% "Promoter",]
	promo <- as.data.table(promo) # Convert to data table
	signal_for <- unique(unlist(sapply(promo[,flank_geneIds], function(x) strsplit(x, ";")))) 
	geneList <- vector("numeric", length(rownames(RNA)))
	names(geneList) <- gff[match(rownames(RNA), gff$ID), "Dbxref"]
	for (i in names(geneList)){
		if (i %in% signal_for){
			geneList[i] <- mean(promo[promo[, flank_geneIds %like% eval(i)], signalValue])
			}
		else {
			geneList[i] <- missing_values
			}
		}
	}
	RNA[mod] <- geneList
	return(RNA)
	}

add_DNA_methylation_data <- function(methylation_data="CCLE_RRBS_TSS1kb_20181022.txt", RNA=RNA_seq, missing_values=NA, mod="Loucy_mC", cell_line="LOUCY_HAEMATOPOIETIC_AND_LYMPHOID_TISSUE"){
	meth <- fread(methylation_data, select = c("locus_id", "CpG_sites_hg19", "avg_coverage", cell_line))
	meth[, SYMBOL := sapply(strsplit(meth[,locus_id], "_"), "[", 1)]
	signal_for <- unique(meth[, SYMBOL])
	geneList <- vector("numeric", length(rownames(RNA)))
	names(geneList) <- gff[match(rownames(RNA), gff$ID), "Name"]
	for (i in names(geneList)){
		if (i %in% signal_for){
			geneList[i] <- mean(meth[meth[, SYMBOL == eval(i)], get(cell_line)])
			}
		else {
			geneList[i] <- missing_values
			}
		}
	RNA[mod] <- geneList
	return(RNA)
}

# Histone methylation datasets were primarilly found through CistromeDB 
Add_all_DNA_and_histone_mod_data <- function(RNA=RNA_seq){ 
	# H3K27me3 (Silencing): From geo/encodeproject.
	RNA <- add_histone_data(peak_file="./Histone_data/H3K27me3_ENCFF048PKS.bed", RNA=RNA, missing_values=0, mod="H3K27me3", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	# H3K9 me2 and me3 (Silencing) From geo/encodeproject. Has data quality problems. Since there were multiple equivalently good files we will use all of them.
	RNA <- add_histone_data(peak_file="./Histone_data/H3K9me2_1_GSM2527574_ENCFF552IXX_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K9me2_1", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	RNA <- add_histone_data(peak_file="./Histone_data/H3K9me2_2_GSM2527573_ENCFF885YUZ_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K9me2_2", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	
	RNA <- add_histone_data(peak_file="./Histone_data/H3K9me3_GSM2534588_ENCFF874YLZ_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K9me3", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	
	# H3K4me1,2,3 Activating marks, (Unknown for me 1): From geo/encodeproject.
	RNA <- add_histone_data(peak_file="./Histone_data/H3k4me1_GSM2527403_ENCFF162ZXR_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K4me1_1", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	RNA <- add_histone_data(peak_file="./Histone_data/H3K4me1_GSM2527404_ENCFF081YKE_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K4me1_2", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	
	RNA <- add_histone_data(peak_file="./Histone_data/H3K4me2_GSM2527553_ENCFF073ZFH_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K4me2_1", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	RNA <- add_histone_data(peak_file="./Histone_data/H3K4me2_GSM2527554_ENCFF353GEA_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K4me2_2", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	
	RNA <- add_histone_data(peak_file="./Histone_data/H3K4me3_GSE96297_ENCFF048PKS_replicated_peaks_GRCh38", RNA=RNA, missing_values=0, mod="H3K4me3_1", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	RNA <- add_histone_data(peak_file="./Histone_data/H3K4me3_GSM2534273_ENCFF797YIG_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K4me3_2", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	# H3K9 and 27 acetylation. Activating marks. From geo/encodeproject.
	RNA <- add_histone_data(peak_file="./Histone_data/H3K9ac_GSM2534139_ENCFF923VCO_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H3K9ac", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	# H3K27 was an independent study avialble from Geo. As such it did not follow the encode format perfectly.
	RNA <- add_histone_data(peak_file="./Histone_data/GSM1916974_LOUCY_H3K27ac_peaks.txt", RNA=RNA, missing_values=0, mod="H3K27ac")
	# H4K20me1 an activating mark
	RNA <- add_histone_data(peak_file="./Histone_data/H4K20me1_GSM2534410_ENCFF980MRC_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H4K20me1_1", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	RNA <- add_histone_data(peak_file="./Histone_data/K4K20me1_GSM2534411_ENCFF804SJN_peaks_GRCh38.bed", RNA=RNA, missing_values=0, mod="H4K20me1_2", col_names=c("peak_name", "score", "strand_1", "signalValue", "pValue", "qvalue", "peak"))
	# Add the DNA methylation data from CCLE
	RNA <- add_DNA_methylation_data(methylation_data="CCLE_RRBS_TSS1kb_20181022.txt", RNA=RNA, missing_values=NA, mod="Loucy_mC", cell_line="LOUCY_HAEMATOPOIETIC_AND_LYMPHOID_TISSUE")
	RNA <- add_DNA_methylation_data(methylation_data="CCLE_RRBS_TSS1kb_20181022.txt", RNA=RNA, missing_values=NA, mod="Sup_T1_mC", cell_line="SUPT1_HAEMATOPOIETIC_AND_LYMPHOID_TISSUE")

	# For the ones with multiple files we will now create averages. Note that since 0 equals no data average will only be taken if both columns > 0. Otherwise the sum will be taken.
	for (i in c("H3K9me2", "H3K4me1", "H3K4me2", "H3K4me3", "H4K20me1")){
		RNA[i] <- vector("numeric", length(rownames(RNA)))
		for (x in rownames(RNA)){
			if (all(as.numeric(RNA[x, colnames(RNA)[colnames(RNA) %in% paste(i, c(1,2), sep="_")]]) == 0)){
				#RNA[x, i] = 0 unecessary since already zero
				next}
			if (TRUE %in% (as.numeric(RNA[x, colnames(RNA)[colnames(RNA) %in% paste(i, c(1,2), sep="_")]]) == 0)){
				RNA[x, i] = sum(as.numeric(RNA[x, colnames(RNA)[colnames(RNA) %in% paste(i, c(1,2), sep="_")]]))
				next}
			else {
				RNA[x, i] = mean(as.numeric(RNA[x, colnames(RNA)[colnames(RNA) %in% paste(i, c(1,2), sep="_")]]))
				}
			}
		}
	return(RNA)
}

Mod_analysis_vs_background <- function(dataset=high_FC$Upregulated_genes, RNA=RNA_seq, mod="H3K27me3", prefix=""){
	#Test out the boxplots etc. Make this into seprate function.
	result <- vector("numeric", length(names(dataset)[names(dataset) %like% "Loucy"]))
	names(result) <- names(dataset)[names(dataset) %like% "Loucy"]
	methylation_list <- list()
	for (i in names(dataset)[names(dataset) %like% "Loucy"]){
		geneList <- RNA[match(gff[match(dataset[[i]], gff$Name), "ID"], rownames(RNA)), mod]
		names(geneList) <- dataset[[i]]
		methylation_list[[i]] <- geneList
		result[i] <- wilcox.test(x=geneList, y=sample(RNA[,mod], size=length(geneList)))$p.value
		}
	methylation_list[["Background"]] <- RNA[,mod]
	svg(paste(prefix, mod, "methylation_boxplot_loucy.svg", sep="_"))
	par(mar=c(18,4,4,4))
	boxplot(x = methylation_list , col = "lightblue", ylab = "Fold Enrichment", frame.plot = FALSE, las=2)
	stripchart(x = methylation_list[seq(1, length(names(methylation_list)) -1)] ,  col="black" , method = "jitter" , pch=19 , vertical = T , add=T)
	title(paste("Loucy", mod, "Methylation"))
	dev.off()
	png(paste(prefix, mod, "methylation_boxplot_loucy.png", sep="_"))
	par(mar=c(18,4,4,4))
	boxplot(x = methylation_list , col = "lightblue", ylab = "Fold Enrichment", frame.plot = FALSE, las=2)
	stripchart(x = methylation_list[seq(1, length(names(methylation_list)) -1)] ,  col="black" , method = "jitter" , pch=19 , vertical = T , add=T)
	title(paste("Loucy", mod, "Methylation"))
	dev.off()
	return(result)
}

Mod_analysis_vs_downregulated <- function(Up=high_FC$Upregulated_genes, Down=high_FC$Downregulated_genes, RNA=RNA_seq, mod="H3K27me3", prefix="./Boxplots/Up_vs_Down"){
	result <- vector("numeric", length(names(Up)[names(Up) %like% "Loucy"]))
	names(result) <- names(Up)[names(Up) %like% "Loucy"]
	methylation_list <- list()
	for (i in names(Up)[names(Up) %like% "Loucy"]){
		genes_Up <- RNA[match(gff[match(Up[[i]], gff$Name), "ID"], rownames(RNA)), mod]
		names(genes_Up) <- Up[[i]]
		genes_Down <- RNA[match(gff[match(Down[[i]], gff$Name), "ID"], rownames(RNA)), mod]
		names(genes_Down) <- Down[[i]]
		methylation_list[[paste(i, "Up", sep="_")]] <- genes_Up
		methylation_list[[paste(i, "Down", sep="_")]] <- genes_Down
		result[i] <- wilcox.test(genes_Up, genes_Down)$p.value
	}
	methylation_list[["Background"]] <- RNA[, mod]
	svg(paste(prefix, mod, "methylation_boxplot_loucy.svg", sep="_"))
	par(mar=c(18,4,4,4))
	boxplot(x = methylation_list, col = "lightblue", ylab = "Fold Enrichment", frame.plot = FALSE, las=2)
	#stripchart(x = methylation_list[names(methylation_list) != "Background"] ,  col="black" , method = "jitter" , pch=19 , vertical = T , add=T)
	title(paste("Loucy", mod, "Methylation"))
	dev.off()
	# and in png format
	png(paste(prefix, mod, "methylation_boxplot_loucy.png", sep="_"))
	par(mar=c(18,4,4,4))
	boxplot(x = methylation_list, col = "lightblue", ylab = "Fold Enrichment", frame.plot = FALSE, las=2)
	#stripchart(x = methylation_list[names(methylation_list) != "Background"],  col="black" , method = "jitter" , pch=19 , vertical = T , add=T)
	title(paste("Loucy", mod, "Methylation"))
	dev.off()
	return(result)
}	

Mod_analysis_transposed <- function(Up=high_FC$Upregulated_genes, Down=high_FC$Downregulated_genes, RNA=RNA_seq, mods=modifications[modifications != "Loucy_mC"], prefix="./Boxplots/Up_vs_Down_transposed"){
	for (i in names(Up)[names(Up) %like% "Loucy"]){
		methylation_list <- list()
		for (mod in mods){
			genes_Up <- RNA[match(gff[match(Up[[i]], gff$Name), "ID"], rownames(RNA)), mod]
			names(genes_Up) <- Up[[i]]
			genes_Down <- RNA[match(gff[match(Down[[i]], gff$Name), "ID"], rownames(RNA)), mod]
			names(genes_Down) <- Down[[i]]
			methylation_list[[paste("Up", mod, sep="_")]] <- genes_Up
			methylation_list[[paste("Down" ,mod, sep="_")]] <- genes_Down
			svg(paste("./Density_plots", paste(mod, i, "density.svg", sep="_"), sep="/"))
			ymax <-  max(density(genes_Up)$y, density(genes_Down)$y)
			xmax <-  max(density(genes_Up)$x, density(genes_Down)$x)
			par(mar=c(4,4,4,10))
			plot(density(genes_Up), col=alpha("red", 0.5),  lwd=2, las=2, main=mod, xlab="Fold Enrichment", 
				 ylim=c(0, ymax + ymax*0.1), xlim=c(0, xmax + xmax*0.1), yaxs="i")
			lines(density(genes_Down), col=alpha("blue", 0.5), lwd=2)
			legend("right", c("Upregulated", "Downregulated"), inset = -.45, xpd=TRUE, col=c("red", "lightblue"), lty = 1, lwd = 2)
			W=wilcox.test(genes_Up, genes_Down)$p.value
			legend("topright", paste("Wilcox P =", as.character(signif(W, 3))))
			dev.off()
			}
		svg(paste(prefix, i, "methylation_boxplot_loucy.svg", sep="_"), width=25, height=14)
		par(mar=c(18,4,4,4), cex.lab=3, cex.axis=3, cex.main=3)  
		boxplot(x = methylation_list, col = "lightblue", ylab = "Fold Enrichment", frame.plot = FALSE, las=2)
		#stripchart(x = methylation_list,  col="black" , method = "jitter" , pch=19 , vertical = T , add=T)
		title(paste(gsub("_", " ", i), "Methylation"))
		dev.off()
		png(paste(prefix, i, "methylation_boxplot_loucy.png", sep="_"), width=1920, height=1080, unit="px")
		par(mar=c(18,4,4,4), cex.lab=3, cex.axis=3, cex.main=3)  
		boxplot(x = methylation_list, col = "lightblue", ylab = "Fold Enrichment", frame.plot = FALSE, las=2)
		#stripchart(x = methylation_list,  col="black" , method = "jitter" , pch=19 , vertical = T , add=T)
		title(paste(gsub("_", " ", i), "Methylation"))
		dev.off()
	}
	return("Done")
}

plot_meth_density <- function(contrast="Sup_T1_VC_100_vs_Control", colum="Sup_T1_mC", filename="Methylation_Expression_density_Sup_T1_VC100.svg",
							  upper_cutoff=0.9, lower_cutoff=0.1){
	high_mC <- rownames(RNA[RNA[!is.na(RNA[, colum]), colum] > upper_cutoff,])
	low_mC <- rownames(RNA[RNA[!is.na(RNA[, colum]), colum] < lower_cutoff,])
	svg(paste("./Density_plots", filename, sep="/"))
	par(mar=c(4,4,4,10))
	plot(density(logFC[rownames(logFC) %in% high_mC, contrast]), col=alpha("red", 0.5), xlim=c(-2,2),  lwd=2, las=2, main=paste(colum, "\n", gsub("_", " ", contrast)), xlab="log2 Fold Change", xaxt="n")
	lines(density(logFC[rownames(logFC) %in% low_mC, contrast]), col=alpha("lightblue", 0.5), lwd=2)
	legend("right", c(colum, paste("No", colum)), inset = -.4, xpd=TRUE, col=c("red", "lightblue"), lty = 1, lwd = 2)
	axis(1, at= seq(-2,2, 0.25))
	W=wilcox.test(logFC[rownames(logFC) %in% high_mC, contrast], logFC[rownames(logFC) %in% low_mC, contrast])$p.value
	legend("topright", paste("Wilcox P =", as.character(signif(W, 3))))
	dev.off()
}

