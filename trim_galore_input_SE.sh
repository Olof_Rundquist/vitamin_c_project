#!/bin/bash -l

# Find all of samples
Replicate1=($(find ./VitC*/Replicate1/*/*fastq.gz -type f | sort -z))
Replicate1_rerun=($(find ./VitC*/Replicate1_rerun/*/*fastq.gz -type f | sort -z))
Replicate2=($(find ./VitC*/Replicate2/*/*fastq.gz -type f | sort -z))
Replicate3=($(find ./VitC*/Replicate3/*/*fastq.gz -type f | sort -z))

mkdir ./trimmed_galore # make the output directories
mkdir ./trimmed_galore/Replicate1
mkdir ./trimmed_galore/Replicate1_rerun
mkdir ./trimmed_galore/Replicate2
mkdir ./trimmed_galore/Replicate3

for index in ${!Replicate1[*]} 
do
    sbatch ./trim_galore_SE.sh ./trimmed_galore/Replicate1 "${Replicate1[index]}" 
done

for index in ${!Replicate1_rerun[*]} 
do
    sbatch ./trim_galore_SE.sh ./trimmed_galore/Replicate1_rerun "${Replicate1_rerun[index]}" 
done

for index in ${!Replicate2[*]} 
do
    sbatch ./trim_galore_SE.sh ./trimmed_galore/Replicate2 "${Replicate2[index]}" 
done

for index in ${!Replicate3[*]} 
do
    sbatch ./trim_galore_SE.sh ./trimmed_galore/Replicate3 "${Replicate3[index]}" 
done
